import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import java.lang.Exception
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CarTests {

    @Test
    fun assertInstanceCreation() {
        val color = CarColor.Grey
        val make = CarMake.Ford
        val fuelConsumption = 9.3f
        val tankCapacity = 50
        val fuelLevel = 30.2f
        val odometer = 10837.98f
        val dailyOdometer = 70.67f

        val car = Car(color, make, fuelConsumption, tankCapacity, fuelLevel, odometer, dailyOdometer)

        assertAll(
            { assertEquals(car.color, color) },
            { assertEquals(car.make, make) },
            { assertEquals(car.fuelConsumption, fuelConsumption) },
            { assertEquals(car.tankCapacity, tankCapacity) },
            { assertEquals(car.getFuelLevel(), fuelLevel) },
            { assertEquals(car.getOdometer(), odometer) },
            { assertEquals(car.getDailyOdometer(), dailyOdometer) }
        )
    }

    @Test
    fun instanceCreationCantBeMoreThanTankCapacity() {
        val tankCapacity = 10
        val startFuelLevel = 15f

        val carCreateFunc = { val car = Car(CarColor.Blue, CarMake.Ford, 5f, tankCapacity, startFuelLevel, 10000f, 100f) }

        Assertions.assertThrows(Exception::class.java, carCreateFunc)
    }

    @Test
    fun refuelChangesValueProperly() {
        val startFuel = 10f
        val fuelAdd = 5f

        val car = Car(CarColor.Blue, CarMake.Ford, 5f, 100, startFuel, 10000f, 100f)
        car.refuel(fuelAdd)

        assertEquals(car.getFuelLevel(), startFuel + fuelAdd)
    }

    @Test
    fun refuelCantAcceptNegativeValue() {
        val car = Car(CarColor.Blue, CarMake.Ford, 5f, 100, 70f, 10000f, 100f)
        val refuelFunc = { car.refuel(-5f) }

        Assertions.assertThrows(Exception::class.java, refuelFunc)
    }

    @Test
    fun refuelFuelCantBeMoreThanTankCapacity() {
        val tankCapacity = 10
        val startFuelLevel = 5f
        val refuel = 6f

        val car = Car(CarColor.Blue, CarMake.Ford, 5f, tankCapacity, startFuelLevel, 10000f, 100f)
        val refuelFunc = { car.refuel(refuel) }

        Assertions.assertThrows(Exception::class.java, refuelFunc)
    }

    @Test
    fun driveCantBeMoreThanFuelPerConsumption() {
        val fuelConsumption = 7f
        val fuelLevel = 20f

        val car = Car(CarColor.Blue, CarMake.Ford, fuelConsumption, 30, fuelLevel, 10000f, 100f)
        val driveFunc = { car.drive(300f) }

        Assertions.assertThrows(Exception::class.java, driveFunc)
    }

    @Test
    fun dailyOdometerMaxCantBeMoreThanOdometerMax() {
        assertTrue(Car.MAX_ODOMETER_VALUE > Car.MAX_DAY_ODOMETER_VALUE)
    }

    @Test
    fun driveCantExceedMaxOdometerValue() {
        val odometer = Car.MAX_ODOMETER_VALUE - 1f

        val car = Car(CarColor.Blue, CarMake.Ford, 1f, 10000, 10000f, odometer, 1f)
        val driveFunc = { car.drive(2f) }

        Assertions.assertThrows(Exception::class.java, driveFunc)
    }

    @Test
    fun driveCantExceedMaxDayOdometerValue() {
        val dayOdometer = Car.MAX_DAY_ODOMETER_VALUE - 1f

        val car = Car(CarColor.Blue, CarMake.Ford, 1f, 10000, 10000f, 1f, dayOdometer)
        val driveFunc = { car.drive(2f) }

        Assertions.assertThrows(Exception::class.java, driveFunc)
    }

    @Test
    fun resetDailyOdometerChangeValueProperly() {
        val car = Car(CarColor.Blue, CarMake.Ford, 1f, 10000, 10000f, 1f, 199f)
        car.resetDailyOdometer()

        assertEquals(car.getDailyOdometer(), 0f)
    }
}