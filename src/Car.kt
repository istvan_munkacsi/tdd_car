import java.lang.Exception

data class Car(
    val color: CarColor,
    val make: CarMake,
    val fuelConsumption: Float,
    val tankCapacity: Int,
    private var fuelLevel: Float,
    private var odometer: Float,
    private var dailyOdometer: Float
) {

    companion object {
        const val MAX_ODOMETER_VALUE = 9999999
        const val MAX_DAY_ODOMETER_VALUE = 99999
    }

    init {
        if(tankCapacity < fuelLevel) {
            // TODO: create custom Exception
            throw Exception("Fuel level can't be more than tankCapacity")
        }
    }

    fun getFuelLevel() = fuelLevel
    fun getOdometer() = odometer
    fun getDailyOdometer() = dailyOdometer

    fun refuel(liters: Float) {
        // TODO: create different custom Exceptions for different cases
        if(liters < 0) {
            throw Exception("Refuel liters can\'t be negative. Passed argument = $liters")
        }

        if(fuelLevel + liters > tankCapacity) {
            throw Exception("Fuel level can\'t become more than tankCapacity")
        }

        fuelLevel += liters
    }

    fun drive(kilometers: Float) {
        val odometerValue = odometer + kilometers
        if(odometerValue > MAX_ODOMETER_VALUE) {
            throw Exception("Odometer max value would be exceeded with $kilometers kilometers")
        }

        val dayOdometerValue = dailyOdometer + kilometers
        if(dayOdometerValue > MAX_DAY_ODOMETER_VALUE) {
            throw Exception("DailyOdometer max value would be exceeded with $kilometers kilometers")
        }

        val maxPossibleDistanceForFuelLevel = fuelLevel / fuelConsumption * 100
        if(kilometers > maxPossibleDistanceForFuelLevel) {
            throw Exception("Can\'t drive $kilometers kilometers with $fuelLevel fuel liters")
        }

        fuelLevel -= kilometers / 100 * fuelConsumption
        odometer += kilometers
        dailyOdometer += kilometers
    }

    fun resetDailyOdometer() {
        dailyOdometer = 0f
    }
}
